Readme file for RPG Heroes console application in C#.

This project is a console backend application of a RPG Hero game.
It is important to understand this game is not a full working application.
This project is just controls some of the backend of a potential RPG Hero game.

The application is written in the programming language C# in the .NET framework.

The application does not have tests for every functionality yet but this will be implemented in the future.

Getting started
---
Feel free to participate in developing this project further.
To get this project yourself you can clone it by pressing the 'clone' button in the top right corner on GitLab.

To work on this project .NET5 or a newer version should be installed
---