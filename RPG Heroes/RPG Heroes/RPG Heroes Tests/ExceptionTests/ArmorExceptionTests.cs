﻿using RPG_Heroes.Enums;
using RPG_Heroes.Exceptions;
using RPG_Heroes.Items;
using RPG_Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes_Tests.ExceptionTests
{
    public class ArmorExceptionTests
    {
        [Fact]
        public void EquipArmorExceptionLevel_ShouldThrowInvalidArmorExceptionBecauseOfLevel()
        {
            HeroAttributes armorAttributes = new HeroAttributes(0, 0, 0);
            Item armor = new Armor("Dusty", 10, Slot.Head, armorAttributes, ArmorType.Cloth);
            Armor arm = ((Armor)armor);
            Heroes hero = new Mage("Harry");

            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(arm));
        }

        [Fact]
        public void EquipArmorExceptionArmorType_ShouldThrowInvalidArmorExceptionBecauseOfArmorType()
        {
            HeroAttributes armorAttributes = new HeroAttributes(0, 0, 0);
            Item armor = new Armor("Dusty", 1, Slot.Head, armorAttributes, ArmorType.Cloth);
            Armor arm = ((Armor)armor);
            Heroes hero = new Warrior("Harry");

            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(arm));
        }
    }
}
