﻿using RPG_Heroes.Enums;
using RPG_Heroes.Exceptions;
using RPG_Heroes.Items;
using RPG_Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes_Tests.ExceptionTests
{
    public class WeaponExceptionTests
    {
        [Fact]
        public void EquipWeaponExceptionLevel_ShouldThrowInvalidWeaponExceptionBecauseOfLevel()
        {
            Item weapon = new Weapon("Rusty", 10, Slot.Weapon, 10, WeaponType.Wand);
            Weapon weap = ((Weapon)weapon);
            Heroes hero = new Mage("Harry");

            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(weap));
        }

        [Fact]
        public void EquipWeaponExceptionWeaponType_ShouldThrowInvalidWeaponExceptionBecauseOfWeaponType()
        {
            Item weapon = new Weapon("Rusty", 1, Slot.Weapon, 10, WeaponType.Bow);
            Weapon weap = ((Weapon)weapon);
            Heroes hero = new Mage("Harry");

            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(weap));
        }
    }
}
