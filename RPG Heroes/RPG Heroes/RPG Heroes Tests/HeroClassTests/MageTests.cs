﻿using RPG_Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace RPG_Heroes_Tests.HeroClassTests
{
    public class MageTests
    {
        [Fact]
        public void Name_Level_AttributesCheck_CreateMage_ShouldReturnCorrectFields()
        {
            //Arrange
            Heroes mage = new Mage("Harry");
            Heroes expected = new Mage("Harry")
            { Level = 1, LevelAttributes = { Strength = 1, Dexterity = 1, Intelligence = 8 }};
            //Act
            Heroes actual = mage;
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUp_MageLevelUp_ShouldReturnCorrectMageLevelAfterLevelUp()
        {
            Heroes mage = new Mage("Harry");
            mage.LevelUp();
            int expected = 2;

            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpAttributes_MageLevelUp_ShouldReturnCorrectHeroAttributesAfterLevelUp()
        {
            Heroes mage = new Mage("Harry");
            mage.LevelUp();
            HeroAttributes expected = new HeroAttributes(2, 2, 13);

            HeroAttributes actual = mage.LevelAttributes;

            Assert.Equivalent(expected, actual);
        }
    }
}
