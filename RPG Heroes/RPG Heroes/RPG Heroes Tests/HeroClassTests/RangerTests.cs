﻿using RPG_Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes_Tests.HeroClassTests
{
    public class RangerTests
    {
        [Fact]
        public void Name_Level_AttributesCheck_CreateRanger_ShouldReturnCorrectFields()
        {
            //Arrange
            Heroes ranger = new Ranger("Harry");
            Heroes expected = new Ranger("Harry")
            { Level = 1, LevelAttributes = { Strength = 1, Dexterity = 7, Intelligence = 1 } };
            //Act
            Heroes actual = ranger;
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUp_RangerLevelUp_ShouldReturnCorrectRangerLevelAfterLevelUp()
        {
            Heroes ranger = new Ranger("Harry");
            ranger.LevelUp();
            int expected = 2;

            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpAttributes_RangerLevelUp_ShouldReturnCorrectHeroAttributesAfterLevelUp()
        {
            Heroes ranger = new Ranger("Harry");
            ranger.LevelUp();
            HeroAttributes expected = new HeroAttributes(2, 12, 2);

            HeroAttributes actual = ranger.LevelAttributes;

            Assert.Equivalent(expected, actual);
        }
    }
}
