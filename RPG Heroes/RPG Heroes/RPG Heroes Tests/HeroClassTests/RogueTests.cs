﻿using RPG_Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes_Tests.HeroClassTests
{
    public class RogueTests
    {
        [Fact]
        public void Name_Level_AttributesCheck_CreateRogue_ShouldReturnCorrectFields()
        {
            //Arrange
            Heroes rogue = new Rogue("Harry");
            Heroes expected = new Rogue("Harry")
            { Level = 1, LevelAttributes = { Strength = 2, Dexterity = 6, Intelligence = 1 } };
            //Act
            Heroes actual = rogue;
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUp_RogueLevelUp_ShouldReturnCorrectRogueLevelAfterLevelUp()
        {
            Heroes rogue = new Rogue("Harry");
            rogue.LevelUp();
            int expected = 2;

            int actual = rogue.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpAttributes_RogueLevelUp_ShouldReturnCorrectHeroAttributesAfterLevelUp()
        {
            Heroes rogue = new Rogue("Harry");
            rogue.LevelUp();
            HeroAttributes expected = new HeroAttributes(3, 10, 2);

            HeroAttributes actual = rogue.LevelAttributes;

            Assert.Equivalent(expected, actual);
        }
    }
}
