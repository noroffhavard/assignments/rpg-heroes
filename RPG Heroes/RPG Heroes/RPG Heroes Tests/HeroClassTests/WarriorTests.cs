﻿using RPG_Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes_Tests.HeroClassTests
{
    public class WarriorTests
    {
        [Fact]
        public void Name_Level_AttributesCheck_CreateWarrior_ShouldReturnCorrectFields()
        {
            //Arrange
            Heroes warrior = new Warrior("Harry");
            Heroes expected = new Warrior("Harry")
            { Level = 1, LevelAttributes = { Strength = 5, Dexterity = 2, Intelligence = 1 } };
            //Act
            Heroes actual = warrior;
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUp_WarriorLevelUp_ShouldReturnCorrectWarriorLevelAfterLevelUp()
        {
            Heroes warrior = new Warrior("Harry");
            warrior.LevelUp();
            int expected = 2;

            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpAttributes_WarriorLevelUp_ShouldReturnCorrectHeroAttributesAfterLevelUp()
        {
            Heroes warrior = new Warrior("Harry");
            warrior.LevelUp();
            HeroAttributes expected = new HeroAttributes(8, 4, 2);

            HeroAttributes actual = warrior.LevelAttributes;

            Assert.Equivalent(expected, actual);
        }
    }
}
