﻿using RPG_Heroes;
using RPG_Heroes.Enums;
using RPG_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes_Tests
{
    public class TotalAttributeTests
    {
        [Fact]
        public void TotalAttributeWithoutArmor_ShouldReturnHeroesTotalAttributesWithoutArmorEquipped()
        {
            Heroes warrior = new Warrior("Hans");
            HeroAttributes expected = new(5, 2, 1);

            HeroAttributes actual = warrior.TotalAttributes();

            Assert.Equivalent(expected, actual);
        }
    }
}