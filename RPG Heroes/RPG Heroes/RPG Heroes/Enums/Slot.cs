﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Enums
{
    public enum Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
