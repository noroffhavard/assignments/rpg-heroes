﻿using RPG_Heroes.Enums;
using RPG_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RPG_Heroes
{
    public class Mage : Heroes

    {   //instanciates a Mage object with given Attributes and valid armor/weapon types
        public Mage(String name) : base(name)
        {
            LevelAttributes.Strength = 1;
            LevelAttributes.Dexterity = 1;
            LevelAttributes.Intelligence = 8;

            ValidArmorTypes.Add(ArmorType.Cloth);

            ValidWeaponTypes.Add(WeaponType.Staff);
            ValidWeaponTypes.Add(WeaponType.Wand);
        }

        //levels up the total level and attribute levels of the Mage
        public override void LevelUp()
        {
            Level += 1;
            LevelAttributes.Strength += 1;
            LevelAttributes.Dexterity += 1;
            LevelAttributes.Intelligence += 5;
        }
    }
}
