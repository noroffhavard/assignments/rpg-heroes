﻿using RPG_Heroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes
{
    public class Ranger : Heroes
    {
        //instanciates a Ranger object with given Attributes and valid armor/weapon types
        public Ranger(String name) : base(name)
        {
            LevelAttributes.Strength = 1;
            LevelAttributes.Dexterity = 7;
            LevelAttributes.Intelligence = 1;

            ValidArmorTypes.Add(ArmorType.Leather);
            ValidArmorTypes.Add(ArmorType.Mail);

            ValidWeaponTypes.Add(WeaponType.Bow);
        }

        //levels up the total level and attribute levels of the Ranger
        public override void LevelUp()
        {
            Level += 1;
            LevelAttributes.Strength += 1;
            LevelAttributes.Dexterity += 5;
            LevelAttributes.Intelligence += 1;
        }
    }
}
