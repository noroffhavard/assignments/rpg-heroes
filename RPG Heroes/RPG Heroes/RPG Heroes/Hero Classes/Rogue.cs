﻿using RPG_Heroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes
{
    public class Rogue : Heroes
    {
        //instanciates a Rogue object with given Attributes and valid armor/weapon types
        public Rogue(String name) : base(name)
        {
            LevelAttributes.Strength = 2;
            LevelAttributes.Dexterity = 6;
            LevelAttributes.Intelligence = 1;

            ValidArmorTypes.Add(ArmorType.Leather);
            ValidArmorTypes.Add(ArmorType.Mail);

            ValidWeaponTypes.Add(WeaponType.Dagger);
            ValidWeaponTypes.Add(WeaponType.Sword);
        }

        //levels up the total level and attribute levels of the Rogue
        public override void LevelUp()
        {
            Level += 1;
            LevelAttributes.Strength += 1;
            LevelAttributes.Dexterity += 4;
            LevelAttributes.Intelligence += 1;
        }
    }
}
