﻿using RPG_Heroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes
{
    public class Warrior : Heroes
    {
        //instanciates a Warrior object with given Attributes and valid armor/weapon types
        public Warrior(String name) : base(name)
        {
            LevelAttributes.Strength = 5;
            LevelAttributes.Dexterity = 2;
            LevelAttributes.Intelligence = 1;

            ValidArmorTypes.Add(ArmorType.Mail);
            ValidArmorTypes.Add(ArmorType.Plate);

            ValidWeaponTypes.Add(WeaponType.Axe);
            ValidWeaponTypes.Add(WeaponType.Hammer);
            ValidWeaponTypes.Add(WeaponType.Sword);
        }

        //levels up the total level and attribute levels of the Warrior
        public override void LevelUp()
        {
            Level += 1;
            LevelAttributes.Strength += 3;
            LevelAttributes.Dexterity += 2;
            LevelAttributes.Intelligence += 1;
        }
    }
}