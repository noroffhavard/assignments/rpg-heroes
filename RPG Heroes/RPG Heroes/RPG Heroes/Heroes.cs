﻿using RPG_Heroes.Enums;
using RPG_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG_Heroes.Exceptions;
using System.Reflection;

namespace RPG_Heroes
{
    /*
     * this class is the parent class of the heroes. A hero has a name, level, attributes, four types of equipment
     * and a list of valid weapon types and armor types.
     */
    public abstract class Heroes
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public HeroAttributes LevelAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; }
        public List<WeaponType> ValidWeaponTypes { get; set; }
        public List<ArmorType> ValidArmorTypes { get; set; }
   
        /*
         * when a hero is created it starts at level 1 with attributes at level 0. The equipments are
         * a weapon, head, body and leg armor.
         */

        public Heroes(String name)
        {
            this.Name = name;
            Level = 1;
            LevelAttributes = new HeroAttributes(0, 0, 0);
            Equipment = new Dictionary<Slot, Item>
            {
                { Slot.Weapon, null },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            ValidWeaponTypes = new List<WeaponType>();
            ValidArmorTypes = new List<ArmorType>();
        }

        public abstract void LevelUp();

        /*
         * equips a weapon to the hero. Takes the weapon as parameter. Throws an exception if the hero is not
         * high enough level and dont have permission to equip weapon. If not the weapon is equiped.
         */
        public void EquipWeapon(Weapon weapon)
        {
            if (Level < weapon.RequiredLevel || !ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException("Your character do not have access to this weapon");
            }
            else
            {
                Equipment.Remove(weapon.Slot); //removes the old weapon if there are any.
                Equipment.Add(Slot.Weapon, weapon); //Equips weapon to weapon slot of hero.

            }
        }

        /*
         * equips an armor item to the hero. Takes the armor item as parameter. Throws an exception if the hero is not
         * high enough level and dont have permission to equip armor. If not the armor is equiped.
         */
        public void EquipArmor(Armor armor)
        {
            if(Level < armor.RequiredLevel || !ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException("Your character do not have access to this armor");
            }
            else
            {
                Equipment.Remove(armor.Slot); //removes the old armor if there are any.
                Equipment.Add(armor.Slot, armor); //equips armor to armor slot of hero.
            }
        }

        /*
         * damage method which returns the heros damage based on what weapon is equiped and the heros attributes
         * each hero has a bonus damage based on a specific attribute level
        */
        public int Damage()
        {
            int weaponDamage = 0;
            if ((Weapon)Equipment[0] != null)
            {
                Weapon weapon = (Weapon)Equipment[0]; // creates weapon object with equiped weapon
                weaponDamage = weapon.WeaponDamage; //passes equiped weapon objects damage to weaponDamage variable
            }

            /*
             * checks if weapon slot of hero is empty. If empty, checks what type of hero it is and
             * gives weapon damage value of 1 and calculates the heros damage based only on hero attributes
             */
            if (!this.Equipment.ContainsKey(Slot.Weapon))
            {
                if (this.GetType() == typeof(Mage))
                {
                    weaponDamage = 1;
                    return weaponDamage * (1 + TotalAttributes().Intelligence / 100);
                }
                if (this.GetType() == typeof(Ranger) || this.GetType() == typeof(Rogue))
                {
                    weaponDamage = 1;
                    return weaponDamage * (1 + TotalAttributes().Dexterity / 100);
                }
                if (this.GetType() == typeof(Warrior))
                {
                    weaponDamage = 1;
                    return weaponDamage * (1 + TotalAttributes().Strength / 100);
                }
            }
            else //else, checks what hero type it is and returns its damage based weapon equiped and attribute level.
            {
                if (this.GetType() == typeof(Mage))
                {
                    return weaponDamage * (1 + TotalAttributes().Intelligence / 100);
                }
                if (this.GetType() == typeof(Ranger) || this.GetType() == typeof(Rogue))
                {
                    return weaponDamage * (1 + TotalAttributes().Dexterity / 100);
                }
                if (this.GetType() == typeof(Warrior))
                {
                    return weaponDamage * (1 + TotalAttributes().Strength / 100);
                }
            }
            return 1; //else returns 1
        }

        /*
         * checks a heros attribute levels + its armor bonuslevels and returns a HeroAttribute object.
         */
        public HeroAttributes TotalAttributes()
        {
            HeroAttributes totalAttributes = new(LevelAttributes.Strength, LevelAttributes.Dexterity, LevelAttributes.Intelligence);

            foreach(Item armor in Equipment.Values) //checks every armor slot that hero has equiped
            {
                if(armor != null && armor.GetType() == typeof(Armor)) //takes the armor if there is an armor at a value in the dictionary
                {
                    Armor arm = (Armor)armor;
                    totalAttributes.Strength += arm.ArmorAttributes.Strength;
                    totalAttributes.Dexterity += arm.ArmorAttributes.Dexterity;
                    totalAttributes.Intelligence += arm.ArmorAttributes.Intelligence;
                }
            }

            return totalAttributes;
        }

        public string Display() //returns a string of hero statistics
        {
            StringBuilder sb = new();

            sb.AppendLine($"Hero name: {Name}");
            sb.AppendLine($"Hero class: {this.GetType().Name}");
            sb.AppendLine($"Hero stats: Total level: {Level} strength: {LevelAttributes.Strength} dexterity: {LevelAttributes.Dexterity} intelligence: {LevelAttributes.Intelligence}");
            sb.AppendLine($"Hero damage: {Damage()}");

            return sb.ToString();
        }
    }
}