﻿using RPG_Heroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Items
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public HeroAttributes ArmorAttributes { get; set; }

        //instanciates an Armor object with two child fields. ArmorAttributes is created with 0 as values.
        public Armor(string name, int RequiredLevel, Slot slot, HeroAttributes armorAttributes, ArmorType armorType) : base(name, RequiredLevel, slot)
        {
            this.ArmorAttributes = new HeroAttributes(0, 0, 0);
            this.ArmorType = armorType;
        }
    }
}