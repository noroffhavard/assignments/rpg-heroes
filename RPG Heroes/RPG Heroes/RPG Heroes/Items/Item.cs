﻿using RPG_Heroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Items
{
    //parent class of Armor and Weapon
    public abstract class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot Slot { get; set; }
        
        //constructor given to child classes. Slot represent what slot an item can be equiped in.
        public Item(string name, int requiredLevel, Slot slot)
        {
            this.Name = name;
            this.RequiredLevel = requiredLevel;
            this.Slot = slot;
        }
    }
}
