﻿using RPG_Heroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Items
{
    public class Weapon : Item
    {
        public int WeaponDamage { get; set; }
        public WeaponType WeaponType { get; set; }

        //instanciates a weapon object. Puts weapon in Slot.weapon.
        public Weapon(string name, int RequiredLevel, Slot slot, int weaponDamage, WeaponType weaponType) : base(name, RequiredLevel, slot)
        {
            this.WeaponDamage = weaponDamage;
            this.WeaponType = weaponType;
            Slot = Slot.Weapon;
        }
    }
}
